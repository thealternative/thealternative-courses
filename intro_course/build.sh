#!/bin/bash
echo "Building pdf..."
pandoc -t beamer --template template.tex --listings slides.md -o slides.pdf --pdf-engine pdflatex \
    && echo "Build successful"
