#!/bin/bash
echo "Building pdf..."
pandoc -t beamer --template template.tex --listings pres.md -o pres.pdf --pdf-engine pdflatex \
    && echo "Build successful"
