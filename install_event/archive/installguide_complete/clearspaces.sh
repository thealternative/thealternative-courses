#!/bin/sh
# This script sets up a somewhat normalized file layout. Please use it before
# adding and commiting changes.

# Get the script's full path
# May not work all the time
pushd . > /dev/null
SCRIPT_PATH="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_PATH}" ]); do
    cd "`dirname "${SCRIPT_PATH}"`"
    SCRIPT_PATH="$(readlink "`basename "${SCRIPT_PATH}"`")";
done
cd "`dirname "${SCRIPT_PATH}"`" > /dev/null
SCRIPT_PATH="`pwd`";
popd  > /dev/null
#echo "script=[${SCRIPT_PATH}]"
#echo "pwd   =[`pwd`]"

DIR="${SCRIPT_PATH}"
BAK=".bak"

if [ "$DIR" = "" ]; then
    exit -1;
else
    # Change 4 spaces to \t
    find "${DIR}" -iname "*tex" -print0 | xargs -0 sed -r -s -i${BAK} 's/\ {4}/\t/g'
    # Clean up any space on end of line
    find "${DIR}" -iname "*tex" -print0 | xargs -0 sed -r -s -i${BAK} 's/\ +$//g'
    # Clean up double spacings
    find "${DIR}" -iname "*tex" -print0 | xargs -0 sed -r -s -i${BAK} 's/\ {2}/\ /g'
    # Change any spacing >1 to single space
    find "${DIR}" -iname "*tex" -print0 | xargs -0 sed -r -s -i${BAK} 's/\ {2,}/\ /g'

    # To remove extraneous files
    #find "${DIR}" -iname "*tex${BAK}" -print0 | xargs -0 rm
fi
