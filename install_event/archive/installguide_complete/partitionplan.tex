This is an overview designed to help you in the partitioning step and teach you some of the Linux file system basics.\\
\important{Please check again whether you have a Legacy setup or use EFI. Check the questionnaire again if you cannot remember.}

\subsubsection{EFI}
This is only relevant if you have a system using EFI. EFI systems require a special boot partition to boot from, called the \emph{ESP (EFI System Partition)}. It will likely already be present on your system as \emph{/dev/sda1} or \emph{/dev/sda2} (see below for more details) when Linux is booted up. If you use EFI but it isn't present then please ask for more help.

\subsubsection{Home partiton}
The home folder is where all your personal files and user configurations are stored. It is possible to move it to a separate partition. That means your files will be physically separated from your system. Most installers do this automatically by default, but you can decide to not use a seperate home partition.

\begin{itemize}
	\item Advantages
		\begin{itemize}
			\item You can easily switch to a different distro and keep all your files and configuration.
			\item It is easier to create a full backup of just your files.
		\end{itemize}
	\item{Disadvantages}
		\begin{itemize}
			\item You have less flexibility in managing your disk space. If you install lots of programs, your root partition might be full even though there's still a lot of space left on your home partition. This would not happen if you did not have a separate home partition. If you only have little disk space to spare for Linux, this can be a real problem.
		\end{itemize}
\end{itemize}

With OpenSUSE, disabling the creation of a separate home partition is easy and straightforward. Refer to \emph{\subsecref{sec:part}{subsec:part-suse}} for more instructions.

In Ubuntu, though, there is no simple option to disable a separate home partition. If you don't want one, you will have to manually partition your disk. Refer to \emph{\subsecref{sec:part}{subsec:part-buntu}} for more instructions. Our helpers will gladly assist you if you're uncomfortable with doing this on your own.

\subsubsection{Swap partiton}
This is a partition which can be used to extend your RAM, and is required for hibernating your laptop. There are no disadvantages in having one, and thus we highly recommend it. Your installer should automatically create one for you but you can decide to not use a swap partition.

\subsubsection{Disk space needed for Linux}
Another decision you have to make here is how much of your disk space you want to dedicate to Linux. This mostly depends on how much you intend to use it and what for.

In theory, Linux can be used with very little space at the expense of functionality. But we want you to experience Linux and many of its features so we came up with the following recommendations. If you have little space to spare then try to give Linux \freespacemin. This should suffice for most but we recommend you give Linux \freespacerec or more to have enough headroom for even the largest of program.

The following 3 aspects can be helpful in your decision:
\begin{enumerate}
	\item \emph{What will you use Linux for?} This is a minimal requirement and indicates how much space you should give at least to Linux. If you just want to play around with Linux without downloading\,/\,installing lots of things, you can decide around 5--10\,GB. However, keep in mind that changing a partition layout is cumbersome\,---\,you might regret this choice later when you start to like Linux more and want to switch to it. If you want to actively use Linux for some time, 20--50\,GB are recommended at least. Finally, if you want to make Linux your main system, you need to additionally allocate enough space to contain all your personal data.
	\item \emph{How large is your drive?} If you have a large hard drive (say, 1 TB or more), space is not very important and you can easily give your Linux a few hundred GB. If your space is limited though, think carefully which system you will use more, then give that system more space. Make sure your existing operating system has at least 10\,GB of free space after shrinking (for installing updates).
	\item \emph{How much space does your existing data take?} The more data you have, the more important it is to know what system to put it on. Make sure your main system has enough space to contain all your data plus the space for the system. Linux and its installed programs usually take up between 5--15\,GB of disk space.
\end{enumerate}

Choosing the right size is tricky because it requires you to anticipate your future behaviour. Ask a helper if you need personal advice.

\subsubsection{The partitions you need}

This is a list of the partitions you need. The installer should automatically figure this out for you\,---\,in most cases you won't need this list. We include it as a reference in case you need to manually set up your partitions.

\begin{itemize}
	\item \textbf{Root partition}
	\begin{itemize}
		\item Create this partition yourself
		\item File system type: \emph{ext4}
		\item Mount point: /
		\item Recommended size: \freespacemin if you want a home partition, otherwise as large as possible.
	\end{itemize}
	\item \emph{Optional:} \textbf{Home partition}
	\begin{itemize}
		\item Create this partition yourself
		\item File system type: \emph{ext4}
		\item Mount point: /home
		\item Recommended size: As large as possible.
	\end{itemize}
	\item \emph{Optional:} \textbf{Swap partition}
	\begin{itemize}
		\item Create this partition yourself
		\item Type: \textit{swap}
		\item Mount point: none
		\item Recommended size: About 2GB more than you have RAM.
	\end{itemize}
	\item \dangersign\emph{EFI only:} \textbf{ESP (EFI System Partition)}
	\begin{itemize}
		\item This partition should already exist and is usually automatically detected\,---\,usually being the first or second partition. You should not change any of its properties except for the mount point.
		\item File system type: \emph{FAT32} (This should already be set. \textsc{Do not format this partition!})
		\item Mount point: /boot/efi
		\item Recommended size: 200MB (This should already be the case. \textsc{Do not change the size.})
	\end{itemize}
\end{itemize}
