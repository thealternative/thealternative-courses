\mysubsection*{Zypper Crashcourse}
\label{subsec:suse-zypper}

- Search for packages: `zypper se <name>`
- Get recent versions: `zypper ref`
- Install a package: `zypper in <package>`
- Update packages: `zypper up`
- Upgrade system: `zypper dup`
- Remove package: `zypper remove <package>`
- Get help: `zypper help`
- zypper shell: `zypper sh` (so you can drop the “zypper” prefix)
- List repos: `zypper lr`
- Add repo: `zypper ar <url> <name>`
- Need more help: `man zypper` & Google

