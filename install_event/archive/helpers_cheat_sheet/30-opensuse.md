Using openSUSE
==============

Booting openSUSE in BIOS / EFI mode
-----------------------------------

-   To check how the USB key has booted compare the screen to the
    pictures below

![image](suse_efi_mode.jpg)

BIOS mode\
![image](suse_bios_mode.jpg)

EFI mode

Installing software in Yast
---------------------------

-   Open Yast via start menu → settings → yast

-   Open “Install Software” (or so)

-   There you can search for keywords and select packages to install

Very small zypper HOWTO
-----------------------

-   Search for packages: \# zypper se &lt;name&gt;

-   Get recest versions: \# zypper ref

-   Install a package: \# zypper in &lt;package&gt;

-   Update packages: \# zypper up

-   Upgrade system: \# zypper dup

-   Remove package: \# zypper remove &lt;package&gt;

-   Get help: \# zypper help

-   zypper shell: \# zypper sh (so you can drop the “zypper” prefix)

-   List repos: \# zypper lr

-   Add repo: \# zypper ar &lt;url&gt; &lt;name&gt;

-   Need more help: \$ man zypper and Google Fu

### Adding the custom repository

-   Our custom repo contains kernels for exotic hardware and some other
    useful stuff.

-   Add it via:\
    \# zypper ar -f
    http://download.opensuse.org/repositories/home:/maxf:/LD/openSUSE\_Leap\_42.1/
    TheAlt

Exotic problems
---------------

### Secure Boot

-   The openSUSE kernel maintained by Max cannot be secure booted!

### Get rid of graphic driver

-   \# echo ‘blacklist nouveau’ tee
    /etc/modprobe.d/my\_cool\_blacklist.conf

-   Maybe add nomodeset:

    -   Edit /etc/default/grub to add ‘nomodeset’ to the kernel cmdline

    -   \# grub2-mkconfig -o /boot/grub2/grub.cfg

### Get rid of grub on Macs and set rEFInd to default

-   \# efibootmgr gives you all boot entries

-   \# efibootmgr -o XXXX,YYYY,ZZZZ sets the boot order. Put refind
    first here

-   \# efibootmgr -b &lt;grub-no&gt; -B deletes the efi entry of grub

### Get broadcom-wl

-   Add packman repo: YaST → Software repos → Add, select community
    repos → Next → select packman → finish

-   \# zypper ref

-   \# zypper install broadcom-wl

### Installation worked fine, but instead of GRUB only Windows is booting

-   Disable secure boot (which would prevent GRUB from chainloading)

-   Find /boot/efi/EFI/Microsoft/Boot/bootmgfw.efi (Windows boot loader)

-   Rename it such that any Linux expert can still find it!

-   Find /boot/efi/EFI/&lt;distro\_name&gt;/grubx64.efi (GRUB)

-   copy GRUB to the location that was previously owned by the windows
    boot loader

-   open the os\_prober file in /etc/grub.d and search for the Windows
    boot entry

-   update the position of the (renamed) windows boot loader and copy
    the text of the menuentry to /etc/grub.d/40\_custom

-   remove os\_prober

-   ’grub-mkconfig -o /boot/grub/grub.cfg’

-   Document all changes so a future linux expert helping our client can
    figure out what you did!

-   In case of a problem ask Maximilian or Jan

