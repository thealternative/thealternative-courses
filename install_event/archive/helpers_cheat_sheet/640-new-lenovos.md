\mysubsection*{openSUSE Install On 2017 Lenovo's}
\label{subsec:sflake-lenovo}

Some newer Lenovo laptops, including the X1 Carbon and the T470s, refuse to boot the OpenSUSE install medium. Any other install media, e.g. ubuntu sticks, work fine.

To get OpenSUSE on these laptops, do the following:

* Ask the Patrol for a refind stick.
* Plug in both the refind stick and the openSUSE install stick.
* Boot the laptop from the refind stick.
* Once refind shows up, press `esc` to refresh the list of boot media.
* You should see an entry for opensuse. Select it and hit enter to boot.
* The OpenSUSE installer should come up now.

### rEFInd Stick Doesn't Work Either

In that case, you can use an ubuntu installer to boot the openSUSE installer:

* Tell the participant to go to the supply desk and request an ubuntu installer (or xubuntu, kubuntu - any kind is fine).
* Plug in both the ubuntu and opensuse sticks.
* Boot from the ubuntu stick. 
* When Grub shows up, press `c` to get to the command line.
* Type `ls`, you should see a list of partitions in the form `(HD0,GPT1)`. Try to guess which disk (HD0, HD1, HD2...) is the openSUSE Installer - they usually have two partitions. 
* Type `set root=(HDX,GPT1)`, X being the number of the disk you think is the installer.
* If you get some error, it was the wrong partition or disk. Reboot and try another.
* The Kernel files reside in `/boot/x86_64/loader/linux` and `/boot/x86_64/loader/initrd`.
* Load the vmlinuz by typing: 
        `vmlinuz /boot/x86_64/loader/linux`
* Load the initial ramdisk by typing: (change path as required)
        `initrd /boot/x86_64/loader/initrd`
* If you can't find the kernel files, you probably loaded the wrong partition or disk. Reboot and try again.
* Type `boot`, hit enter.
* If all goes well, the openSUSE installer now boots. Install Linux as normal.

### Booting, But "Invalid Null Terminator String" Error Or "Invalid Repository" Error

In this case, the OpenSUSE install disk is bad. Ask the user to go to the Supply Desk and exchange the usb installer. 

Note that the USB Installer works fine for most other devices - only Lenovos show this kind of error.

\cleartooddpage
