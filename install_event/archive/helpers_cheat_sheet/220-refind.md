\mysubsection*{Installing rEFInd}
\label{subsec:mac-refind}

*(tested on Macbook Pro 13-inch, Early 2011)*

The installation of rEFInd fails under El Capitan (Mac OS X 10.11). The reason is 
the activated system integrity protection ('rootless', more info here: 
http://www.rodsbooks.com/refind/sip.html). Ignoring the error message (ALERT: 
SIP ENABLED) will result in the files being copied into the ESP but the device 
not being able to start from it. This is because the NVRAM variables weren't 
properly adjusted.

### Workaround 1: Clemens' Suggestion

Reference: [http://www.rodsbooks.com/refind/installing.html#manual_renaming](http://www.rodsbooks.com/refind/installing.html#manual_renaming)

- Ignore SIP error, attempt installation with `Y`
- Open a terminal and enter (maybe using sudo):
    - `diskutil list` (find ESP, usually `disk0s1`)
    - `diskutil mount /dev/disk0s1`
    - `cd /Volumes/EFI/EFI`
    - `mv refind BOOT`
    - `cd BOOT`
    - `mv refind_x64.efi bootx64.efi`
- Restart, hold down [Alt] and pick `EFI BOOT` for rEFInd

Advantage: Mac detects rEFInd automatically on the ESP, survives NVRAM resets
and no need to boot into recovery. 

Disadvantage: Mac OS X boots per default, not rEFInd

Solution:  Boot into recovery [Option + R] -> Utilities -> Terminal
    
- `diskutil mount /dev/disk0s1`
- `bless --mount /Volumes/EFI --setBoot --file /Volumes/EFI/EFI/BOOT/bootx64.efi --shortform`

Refind should boot automatically henceforth.

Should Recovery not exist then use a bootable installer [https://support.apple.com/en-us/HT201372](https://support.apple.com/en-us/HT201372).


### Workaround 2: Recommended By rEFInd

Reference: [http://www.rodsbooks.com/refind/sip.html#disable](http://www.rodsbooks.com/refind/sip.html#disable) 

- Boot recovery
- Do one of the following:
    - Install rEFInd from there **OR**
    - `csrutil disable`
    - Reboot into Mac OS X
    - Install rEFInd
    - Reboot into recovery
    - `csrutil enable`

On newer MacBooks (Clemens' Early 2011 doesn't have an `csr-active-config` 
entry, `nvram -p` for listing all available entries) it is also possible to 
toggle the SIP state with rEFInd. Mount ESP, follow **Workaround 1** and
finally edit `refind.conf` (see: [http://www.rodsbooks.com/refind/sip.html#refind_manage](http://www.rodsbooks.com/refind/sip.html#refind_manage)).

\pagebreak
