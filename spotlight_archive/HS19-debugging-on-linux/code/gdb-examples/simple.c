
#include<stdio.h>
#include<stdbool.h>
#include<tgmath.h>

bool
is_prime(int number) {
  int max = ((int) sqrt((double) number)) + 1;
  for (int i = 2; i < max; i++)
	if (!(number % i))
	  return false;
  return true;
}

int
main(int argc, char** argv) {
  int number;
  scanf("%d", &number);
  if (is_prime(number))
	printf("%d: prime\n", number);
  else 
	printf("%d: not prime\n", number);
  return 0;
}
