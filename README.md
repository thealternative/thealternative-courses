# Courses

This is a public repository that contains a collection of our current (and past) LinuxDays courses.

Introduction:
- [Free and Open Source Software (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/FOSS_course/slides.pdf)
- [Introduction to Linux (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/intro_course/slides.pdf)
- [Install Guide](https://gitlab.ethz.ch/thealternative/courses/-/blob/master/install_guide_updated/master/install_guide_updated.pdf)
- [Bash Guide](https://thealternative.ch/guides/bash.php)

Console & Bash:
- [Console Toolkit Part 1 (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/console_toolkit/console_toolkit_1.pdf)
- [Console Toolkit Part 1&2 (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/console_toolkit/console_toolkit_2.pdf)
- [Console Toolkit Exercises (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/console_toolkit/exercise_files/exercises.pdf)
- [Bash Course (Slides) (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/bash_course/pres/pres.pdf)
- [Bash Course (Guide) (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/bash_course/guide/guide.pdf)
- [Bash Course Exercises (.pdf)](https://gitlab.ethz.ch/thealternative/courses/blob/master/bash_course/exercisesheet/sheet.pdf) and [Solutions](https://gitlab.ethz.ch/thealternative/courses/blob/master/bash_course/exercisesheet/sol.pdf)

Tools:
- [Git](https://thealternative.ch/courses/git/git.html)

# Cloning the repository

To download this repository, please install [git-lfs](https://git-lfs.github.com/). You can clone (download) this entire repository by issuing the following command in a terminal:

```
git-lfs install
git clone https://gitlab.ethz.ch/thealternative/courses.git
```

You can find the course materials in the various subdirectories of this repository.

You need `git-lfs` installed for the larger files to download as expected.

# For TheAlternative Members

This repository is kept up to date by TheAlternative members. If you teach a course, please add your slides and supplementary material, such as exercise sheets, to this repo. Do not upload any private files, as this repository is publicly visible.
