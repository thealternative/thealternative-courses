# Project management

How to manage an IT project. 

Main objectives:
- Know what makes a good project: High-level view about a suitable environment for you and your project.
- Know how to manage a project: Concrete resources how to start, execute and finish a project.
