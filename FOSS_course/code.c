#include <stdio.h>
#define N 10 //Larger N for slower learners

int main()
{
    int i;

    puts("Repeat after me:");

    for (i = 0; i < N; i++)
    {
        puts("FOSS is also great for non-programmers!");
    }

    return 0;
}
