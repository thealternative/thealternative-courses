<!DOCTYPE html>
<html>
  <head>
    <title>Linux Toolkit</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="css/remark.css">
    <link rel="stylesheet" media="print" href="css/remark.print.css">
  </head>
  <body>
    <textarea id="source">

class: center, middle

# Slides

<img src="images/slides_QR.png" width="50%">

<a> http://0x0.st/N0oX</a>

Download them from the link, then open `index.html` in your browser

---

class: center, middle

# Linux Toolbox

Nils Leuzinger

<img src="images/thealternative-logo.jpg" width="30%">


---

<!--


# We will see

...a short **Bash** introduction, to get the necessary intuition for what most tools in this course do.

...how most classic Linux tools **compose**.

...a wide spectrum of available **tools** and what you can do with them.

-->

# Goals

Reduce unknown unknowns (`what's available?`, `what do I search for?`, etc).

Give a rough intuition for what classic Linux tools can do.

Learn vocabulary.

Provide pointers for where to learn the tools.


---

<!--
# Example: a download filer

Displays the most recently downloaded file and asks what to do with it

```bash
file=$(ls -tp1 ~/Downloads | grep -v "/$" | head -1)
dirs='Images,Videos,Documents,Documents/books,DELETE,OPEN'
choice=$(echo $dirs | rofi -dmenu -sep ',' -p "move '$file' where? ")
[[ "$choice" = "" ]] && exit
[[ "$choice" = DELETE ]] && rm ~/Downloads/"$file" && exit
[[ "$choice" = OPEN ]] && xdg-open ~/Downloads/"$file" && exit
mv ~/Downloads/"$file" "$choice"
```

Save the above in `/home/nils/filer.sh` and add a few lines of text in `~/.xbindkeysrc`:

```bash
"bash /home/nils/filer.sh"
  control+shift+f
```

# How to use this talk TODO

- pick and choose the tools that seem interesting to you and try to learn them
- come back to the slides and use them as a glossary for unknown words

-->

# Some philosophy before Bash

Many of the classical Linux tools adhere to the "UNIX principle":

<blockquote>Do one thing and do it well.</blockquote>

This usually implies that they are built with composability in mind.  
We like that because it means we learn more things by doing less.

---

# Bash

Related words: terminal, terminal emulator, shell, command line, ...

<img src="images/bash.png" width="50%">

In a nutshell:  
Bash is a tool that let's you interact with your computer via text commands.

**Try it out:** Open a terminal (just search for `Terminal` in your launcher)

---

# Bash Commands

Bash let's you interact with your computer by typing commands.

There are **many** commands. Some examples:  
`ls`, `pwd`, `cd`, `echo`, `mv`, `cat`, `alias`, ...

Some commands are built into Bash, others can be downloaded.

Some commands are really just programs: try typing `firefox` and hitting Enter if you have it installed.

Most commands take options that modify their default behavior.  
`df` versus `df -h`

---

# Basics: echo

<video width="320" height="240" controls>
  <source src="demos/demo1-2021-04-15_23.14.09.mkv.mp4" type="video/mp4">
</video>

---

# Basics: Taking a Walk

List files and show where we are:

```bash
ls
pwd
```

Enter and leave directories:

```bash
cd linuxdaysDemo
cd ..
cd /home/nils/Desktop
cd ~/Desktop
```

---

# Demo: ls, cd

 <video width="320" height="240" controls>
  <source src="demos/demo2-2021-04-15_23.12.20.mkv.mp4" type="video/mp4">
</video>

---

# Demo: rm, cat

 <video width="320" height="240" controls>
  <source src="demos/demo2_1-2021-04-15_23.32.44.mkv.mp4" type="video/mp4">
</video>

---

# Basics: Options

 <video width="320" height="240" controls>
  <source src="demos/demo3-2021-04-15_23.14.40.mkv.mp4" type="video/mp4">
</video>


---

# Bash terminology

The **terminal** is the window in which bash runs.

Bash is a kind of **shell**. There are others (I use Zsh).


<img src="images/terminal_old.webp" alt="https://www.reddit.com/r/raspberry_pi/comments/6se1qy/old_terminal_connected_to_my_pi_emulating_a_modem/" width="60%">  
An actual terminal


---

# Bash for programming

Bash can even be used as a (terrible) programming language, using commands like `for`, `if`, `while`, `[[` etc.  

It also makes use of special constructs that aren't commands  
(e.g. arithmetic expansion: `echo $((5+3))`)

We'll only bother with `|` and `>` ("piping" and "redirecting").

---

# | and >

Two incredibly useful Bash features!

Both redirect command output:  
- `|` redirects to a new command ("piping")
- `>` redirects into a file ("file redirection" or "redirection" for short)

<img src="images/catpiperedirection.png" width="60%">

<p style="color: #BBBBBB;">(you're not supposed to pipe cat's output but no one cares)</p>

---

# Demo

 <video width="320" height="240" controls>
  <source src="demos/demo4-2021-04-15_23.27.52.mkv.mp4" type="video/mp4">
</video>

---

# Shell scripts

Shell scripts are just textfiles.
We can put commands into them and then execute them all at once.

Let's use the editor **nano** to create a file:

```bash
nano ~/betterCd.sh
```

(inside nano:)
```bash
#!/bin/sh
cd "$1"
ls
```

Exit nano by pressing Control-o, Enter, and Control+x.  
Then execute:

````bash
. ~/betterCd.sh ~/Documents
```

If you want, add `alias c='. ~/betterCd.sh'` to the file `~/.bashrc`.

---

# Demo

 <video width="320" height="240" controls>
  <source src="demos/demo5-2021-04-15_23.59.13.mkv.mp4" type="video/mp4">
</video>

---

# Bash Summary

Bash is a different way of interacting with your system

Bash runs in your terminal.

Bash is a shell. There are others: Zsh, fish, Dash, csh

Resources: 
- our bash guide (https://thealternative.ch/guides/bash.php)
- Lhunath and GreyCat's bash guide (http://mywiki.wooledge.org/FullBashGuide)

---

# Teaser

Move all `.png` files from Downloads to Images:  
```bash
mv ~/Downloads/*.png ~/Images
```

Go to our images, make a new directory, and move newest five images there:
```bash
cd ~/Images
mkdir newImages
ls -1t *.png | head -5 | xargs -I {} mv {} newImages
```

Rename all images in the current folder to `*_with_family`:  
```bash
for file in *.png; do
        mv "$file" "$file_with_family"
done
```

---

# Some final tips

Use the Tab key for autocompletion (hit twice for all options)

Control+c terminates the current command

You can recall recently used commands by using the arrow keys.

Sometimes you don't know how to use commands. `man command` can help with that.  
You can search it with /, and quit it with Q.

---

class: center, middle
# Overview

<img src="images/category1.jpg" width="70%">

---

class: center, middle

# Tool Galore

We will now talk about multiple tools and how to combine them.

---

# Scripting Languages

Bash is not suited for programming.  
For more complex jobs, a real programming language is needed.

Two common choices:  
- **Python**: Recommended for beginners. Lots of built-in functionality.
- **Ruby**: If you already program. Compact syntax & metaprogramming.

These fill niches that bash doesn't, and are still close to the system.


<img src="images/polygon.png" width="50%">

---

class: center, middle
# Overview

<img src="images/category1.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category2.jpg" width="70%">

---

# Notifications

Typically, linux has a notification daemon integrated.

```bash
notify-send "Hello"
```

This is often useful for things that you want to run in the background.  

Example: Timer

```bash
sleep 300; notify-send "Time is up!"
```

---

# Demo

 <video width="320" height="240" controls>
  <source src="demos/demo6-2021-04-16_00.06.16.mkv.mp4" type="video/mp4">
</video>

---

class: center, middle
# Overview

<img src="images/category2.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category3.jpg" width="70%">

---

# Rofi

Can be used to display a list of things  
and then let's us choose one of them.

```bash
echo "one,two,three" | rofi -sep ',' -dmenu
```

Rofi's dmenu mode on its own is not useful, but can serve as a powerful launcher
when combined with other commands:  

```bash
ls -1 ~/books/*.pdf | rofi -dmenu | xargs -I {} xdg-open {}
```

See: https://github.com/davatorium/rofi

---

# Demo

 <video width="320" height="240" controls>
  <source src="demos/demo7-2021-04-16_00.18.41.mkv.mp4" type="video/mp4">
</video>

---

class: center, middle
# Overview

<img src="images/category3.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category4.jpg" width="70%">

---

# xbindkeys

A **keydaemon**! Let's us configure keyboard shortcuts.

In file `~/.xbindkeysrc`:

```bash
"notify-send test"
  control+t

"ls -1 ~/books/*.pdf | rofi -dmenu | xargs -I {} xdg-open {}"
  control+Mod1+b
```

Only works when the daemon is running: `xbindkeys`

Might be useful to add it to the startup file:  
`echo xbindkeys >> ~/.xsessionrc`

---


```bash
# pick most recently downloaded file. Store in a variable.
file=$(ls -tp1 ~/Downloads | grep -v "/$" | head -1)
# create ,-separated list of targets
dirs='~/Images,~/Videos,~/Documents,~/Documents/books,DELETE,OPEN'

# Display rofi to choose from actions. Store in variable.
choice=$(echo $dirs | rofi -dmenu -sep ',' -p "move '$file' to: ")
[[ "$choice" = "" ]] && exit

# if the choice was DELETE, then delete and exit
[[ "$choice" = DELETE ]] && rm ~/Downloads/"$file" && exit

# if the choice was OPEN, then open and exit
[[ "$choice" = OPEN ]] && xdg-open ~/Downloads/"$file" && exit

# otherwise move the file
mv ~/Downloads/"$file" $choice
```

Save the above in `/home/nils/filer.sh` and add a few lines of text in `~/.xbindkeysrc`:

```
"bash /home/nils/filer.sh"
  control+shift+f
```

---

class: center, middle
# Overview

<img src="images/category4.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category5jpg.jpg" width="70%">

---


# grep

A command to look for **regular expressions** (commonly just "regex").

Think of regexes as being text patterns on steroids:

`grep -P '^[0-9]' data.csv` prints all the lines starting with a number

We can put multiple greps together to chain filters!

---

# Demo

 <video width="320" height="240" controls>
  <source src="demos/demo8-2021-04-16_01.29.29.mkv.mp4" type="video/mp4">
</video>


---

#grep

Hot tip: Put `alias grep='grep -P'` into `~/.bashrc`.  

`-P` switches to "Perl regular expressions".

Unfortunately, there are lots of regex standards with minor differences.  
Stick with one and don't worry about it (until you do).

https://www.rexegg.com/

---

#grep

Hot tip: Put `alias grep='grep -P'` into `~/.bashrc`.  

`-P` switches to "Perl regular expressions".

Unfortunately, there are lots of regex standards with minor differences.  
Stick with one and don't worry about it (until you do).

https://www.rexegg.com/

<img src="images/xkcd.png" width="50%">
<p style="color: grey;">https://xkcd.com/1171/</p>

---

class: center, middle
# Overview

<img src="images/category5jpg.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category6.jpg" width="70%">

---

# find

A general purpose tool for finding various files according to search filters.

Especially nice if used with the `-exec` option or regular expressions:

Remove all files (`-type f`) that end in .php (`*.php`):  
```bash
find . -type f -name "*.php" -exec rm {} \;
```

Display all files that contain a comment that contains `TODO`:  
```bash
find ~/Documents -type f -exec grep -l "//.*TODO" {} \; 
```

---

class: center, middle
# Overview

<img src="images/category6.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category7.jpg" width="70%">

---

# sed

The **s**tream **ed**itor.

This tool can search for regular expressions like we did with `grep`  
and perform various operations on the matched lines.

Example: Delete all comments from a file
```bash
sed -P 's/^#.*//g' file.py
```

---

class: center, middle
# Overview

<img src="images/category7.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category8.jpg" width="70%">

---


# File Conversion

Sometimes you want to convert between similar file formats.  
There are a lot of tools for this.  
Most are specific for some kind of file content.  
Here are some common ones:

- `ffmpeg` for audio and video
- `pandoc` for many hierarchical text formats (mark down, html, latex)
- `convert` for image files. Also known as ImageMagick.

<hr>

Example: I had to convert all demo files from `.mkv` to `.mp4`:
```bash
cd demos
for file in *.mkv; do
        ffmpeg -i "$file" "${file%.*}.mp4"
done
```

(what `ffmpeg` sees: `ffmpeg -i demo1.mkv demo1.mp4`)

---

class: center, middle
# Overview

<img src="images/category8.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category9.jpg" width="70%">

---

# wget

A general purpose download tool.

Mirror a webpage:
```bash
wget --mirror --convert-links --page-requisites \
--no-parent -e robots=off https://thealternative.ch
```

Download all pdf files:
```bash
site=https://people.inf.ethz.ch/suz/teaching/252-0210.html
wget --no-parent -r -l 1 -A .pdf $site
```

---

class: center, middle
# Overview

<img src="images/category9.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category10.jpg" width="70%">

---

# git

Git is a version control system:  
Keep track of who changed what, when and why, and revert changes easily. 

Clone our courses repository:
```bash
git clone https://gitlab.ethz.ch/thealternative/courses.git
```

When you just want a pull changes:
```bash
git stash
git pull
```

Recommended: Git for ages 4 and up (video)

---

class: center, middle
# Overview

<img src="images/category10.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category11.jpg" width="70%">

---

# ssh

SSH lets us connect to servers and execute commands there.

Connect to `Euler`, an ETH supercomputer:

```bash
ssh nilsl@euler.ethz.ch
```

We land in a bash shell and can execute commands.

The command **scp** let's you copy files between the server and your PC.
There's also **rsync** which is a more advanced way of copying files.
---

class: center, middle
# Overview

<img src="images/category11.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category12.jpg" width="70%">

---

# tmux

Execute long-running processes in the background while your terminal is closed.
Especially useful in combination with SSH.

```bash
ssh nilsl@euler.ethz.ch
tmux new -s my_session
tmux attach my_session
```

Do some stuff, close the window, then come back later.

```bash
ssh nilsl@euler.ethz.ch
tmux attach my_session
```

An older but more widely available alternative to tmux is **screen**

---

class: center, middle
# Overview

<img src="images/category12.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category13.jpg" width="70%">

---

# xdotool


Automates key presses and mouse movements.
Useful for repetitive user interface stuff.

In a browser, copy the link of the current tab and then close it:
```bash
xdotool key F6
sleep 0.2
xdotool key ctrl+c
sleep 0.2
xdotool key ctrl+w
```

Of course you can bind this to a shortcut with **xbindkeys**!
```bash
"xdotool key F6; sleep 0.2; xdotool key ctrl+c; sleep 0.2; xdotool key ctrl+w"
  F10
```

---

# xdotool

How I made the demos:

```bash
#!/bin/bash
keypause=200
commandpause=2

xdotool key --clearmodifiers ctrl+l sleep 2 key ctrl+alt+r sleep 2
while read line
do
        xdotool type --delay $keypause "$line"
        xdotool key Return
        sleep $commandpause
done < $1
sleep 2
xdotool key ctrl+alt+r
xdotool type "cd ~/linuxdaysDemo"
xdotool key Return
```

---

# Demo: xdotool

 <video width="320" height="240" controls>
  <source src="demos/demo9-2021-04-16_01.13.18.mkv.mp4" type="video/mp4">
</video>


---

class: center, middle
# Overview

<img src="images/category13.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category14.jpg" width="70%">

---

# Vim

The best invention since sliced bread. Observe:

Composable shortcuts and modal editing!  
Tree-based undo with timetravelling!  
Can edit files remotely via SSH!  
Recursive keyboard macros!  
Takes weeks to learn!  
30 years old!

<img src="images/vimlogo.png" width="40%">

---

# Vim

Use **Neovim**. It's a rewrite and has nice defaults.  

After you have installed Vim, execute `vimtutor`.  
This shows you the basics.

If you want to learn more after that, I recommend the user manual:  
Type `:h` in Vim, then press Enter.  
You can follow the links in Vim by positioning the cursor over them  
and pressing  `ctrl+]`.

---

# Vim

Use **Neovim**. It's a rewrite and has nice defaults.  

After you have installed Vim, execute `vimtutor`.  
This shows you the basics.

If you want to learn more after that, I recommend the user manual:  
Type `:h` in Vim, then press Enter.  
You can follow the links in Vim by positioning the cursor over them  
and pressing  `ctrl+]`.

<img src="images/vim.png" width="50%">

---

class: center, middle
# Overview

<img src="images/category14.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category15.jpg" width="70%">

---

class: center, middle
# Overview

<img src="images/category16.jpg" width="70%">

---

# Honorable mentions

**stow** lets you manage config files easily.  
**ranger** is a console-based file manager.  
**i3** is an alternative window manager.  (<a href="https://events.thealternative.ch/e/window-manager-workshop">come to our workshop in 2 weeks</a>)  
**Borg** is a great backup tool.  

---

---

# On analysis paralysis

Having so many choices sometimes makes it hard to start.

If that's the case for you: Check out our Bash guide.  

If you want some accompanying exercises,  
check out our past Console Toolkit course:  
https://gitlab.ethz.ch/thealternative/courses/-/blob/master/console_toolkit/exercise_files/exercises.pdf

---

# Final notes

Look for "Linux ricing" on the internet if you need inspiration.

Get the course slides on  
https://thealternative.ch

<img src="images/slides.jpg" width="50%"/>


---

# Next up

Come to the Bash talk on Thursday, 17:15 (<a href="https://events.thealternative.ch/e/the-alternative-bash-HS21">register</a>).

It would be great if you could fill out the feedback:  
https://feedback.thealternative.ch/

---

---

# Bash Mnemonics

**echo** - gives back same thing, like an echo
**ls** - list  
**pwd** - present working directory  
**cd** - change directory  
**rm** - remove  
**cat** - con**cat**enate (originally mainly used to concatenate files)  



    </textarea>
    <script src="js/remark.min.js" type="text/javascript"></script>
    <script src="js/remark.driver.js" type="text/javascript"></script>
  </body>
</html>
